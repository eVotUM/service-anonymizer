# eVote Anonymizer Service

## Table of contents

1. [Dependencies](#dependencies);
2. [Starting developement](#starting-developement);
3. [Running tests](#running-tests).

## Dependencies

- [Docker Engine](https://docs.docker.com/engine/installation/);
- [Docker Compose plugin](https://docs.docker.com/compose/install/linux/#install-using-the-repository);
- [uv](https://docs.astral.sh/uv/getting-started/installation/).

## Starting developement

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1. Start by cloning the project to your local machine:

   ```shell
   $ git clone git@gitlab.com:eVotUM/service-anonymizer.git
   ```

2. Enter the cloned folder and start `docker-compose` with `--build` flag:

   ```shell
   $ cd service-anonymizer
   $ docker compose up --build
   ```

3. After all this steps, the container are available on the following address:

- [Anonymizer API](https://127.0.0.1:8000/api/v1/);

### Configure local development

1. Intall the following tools:

   if you are using ubuntu:

   ```shell
   $ sudo apt install libmariadb-dev clang
   ```

   if you are using fedora:

   ```shell
   $ sudo dnf install libmariadb-dev clang
   ```

1. Access to project root dir and install dependencies locally:

   ```shell
   $ uv sync
   ```

1. Install the `pre-commit` hooks:

   ```shell
   $ uv run pre-commit install
   ```

1. To activate intelisense on VS Code, you need to manually choose the `python.defaultInterpreterPath` to use the path of virtualenv created by the previous step. The path will look like `./.venv/bin/python`.

## Running tests

### Python

To run tests you need to enter on **`evoteanonymizer` container**:

```bash
$ docker compose exec app bash
```

Run one of the following examples code:

```bash
# Linting tests
$ make lint

# Unit tests
$ make test

# Code formatters
$ make lint-format
```
