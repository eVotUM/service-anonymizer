# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import codecs
import logging
import os
import sys
from pathlib import Path

LICENSE = """eVotUM - Electronic Voting System
Copyright (c) 2020 Universidade do Minho
Developed by Eurotux (dev@eurotux.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>."""

PROJECT_NAME = "evoteanonymizer"
BASE_DIR = Path(__file__).resolve().parent.parent
PROJECT_DIR = BASE_DIR.joinpath(PROJECT_NAME).resolve()

# relative to PROJECT_DIR
EXCLUDE = ["static/vendor/"]

logger = logging.getLogger(__name__)


def main(argv):
    dirpath = os.path.realpath(PROJECT_DIR)
    for root, _dirs, filenames in os.walk(dirpath):
        for filename in filenames:
            filepath = Path(root).joinpath(filename)
            if not to_exclude(filepath):
                extension = Path(filename).suffix.lower()
                if extension == ".py":
                    logger.info("Prepending license: %s", filepath)
                    prepend_license(filepath, get_license())
                elif extension == ".js":
                    logger.info("Prepending license: %s", filepath)
                    prepend_license(filepath, get_license("// "))


def to_exclude(filepath):
    for exclude in EXCLUDE:
        to_exclude = PROJECT_DIR.joinpath(exclude).resolve()
        if str(to_exclude) in str(filepath):
            return True
    return False


def prepend_license(filepath, license_data, prepend=""):
    with codecs.open(filepath, "r+", "utf-8") as f:
        content = f.read()
        if "# -*- coding: utf-8 -*-" in content:
            content = "".join(content.splitlines(True)[1:])
        f.seek(0, 0)
        to_prepend = "{0}\n\n{1}"
        if prepend:
            to_prepend = prepend + to_prepend
        f.write(to_prepend.format(license_data, content))


def get_license(prefix="# "):
    lines = [(prefix + line).strip() for line in LICENSE.splitlines()]
    return "\n".join(lines)


if __name__ == "__main__":
    main(sys.argv[1:])
