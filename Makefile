.PHONY: help test test-ci lint lint-check lint-format lint-fix lock


help:
	@echo "Available commands:"
	@echo "  make test           - Run tests with coverage report"
	@echo "  make lint           - Run lint check and format check"
	@echo "  make test-ci        - Run tests with coverage report for CI"
	@echo "  make lint-check     - Run lint check only (ruff)"
	@echo "  make lint-format    - Check code formatting"
	@echo "  make lint-fix       - Fix linting and formatting issues"
	@echo "  make lock           - Update uv lock"

test:
	uv run coverage run runtests.py && coverage report && coverage erase

test-ci:
	uv run coverage run runtests.py && uv run coverage report && uv run coverage xml

lint: lint-check lint-format

lint-check:
	uv run ruff check .

lint-format:
	uv run ruff format . --check
	uv run pyproject-fmt pyproject.toml --check

lint-fix:
	uv run ruff check . --fix
	uv run ruff format .
	uv run pyproject-fmt pyproject.toml

lock:
	uv lock
