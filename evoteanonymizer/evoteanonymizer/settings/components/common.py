# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from decouple import Csv, config
from get_docker_secret import get_docker_secret

from evoteanonymizer.settings.components import RELEASE

DEBUG = False

SECRET_KEY = get_docker_secret("DJANGO_SECRET_KEY", safe=False)
ALLOWED_HOSTS = config("DJANGO_ALLOWED_HOSTS", default="*", cast=Csv())
RELEASE_VERSION = RELEASE


# Application definition
DJANGO_APPS = ["django.contrib.contenttypes"]

THIRD_PARTY_APPS = ["django_extensions", "drf_spectacular", "rest_framework_api_key", "rest_framework"]

LOCAL_APPS = ["core"]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


# MIDDLEWARE CONFIGURATION

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
]

ROOT_URLCONF = "evoteanonymizer.urls"

TEMPLATES = [{"BACKEND": "django.template.backends.django.DjangoTemplates", "APP_DIRS": True}]

WSGI_APPLICATION = "evoteanonymizer.wsgi.application"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


# SECURITY - https://docs.djangoproject.com/en/dev/ref/middleware/#module-django.middleware.security

SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = False
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_SECONDS = 31536000
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
X_FRAME_OPTIONS = "DENY"


# DRF

REST_FRAMEWORK = {
    "TITLE": "eVoto Anonymizer API",
    "DESCRIPTION": "eVoto Anonymizer API documentation.",
    "VERSION": "1.0.0",
    "SERVE_INCLUDE_SCHEMA": False,
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
    "UNAUTHENTICATED_USER": None,
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_RENDERER_CLASSES": ["rest_framework.renderers.JSONRenderer"],
    "DEFAULT_AUTHENTICATION_CLASSES": [],
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework_api_key.permissions.HasAPIKey"],
    "DEFAULT_THROTTLE_CLASSES": [
        "rest_framework.throttling.AnonRateThrottle",
        "rest_framework.throttling.UserRateThrottle",
    ],
    "DEFAULT_THROTTLE_RATES": {"anon": "120/min", "user": "120/min"},
    "APPEND_COMPONENTS": {
        "securitySchemes": {"ApiKeyAuth": {"type": "apiKey", "in": "header", "name": "Authorization"}}
    },
    "SECURITY": [{"ApiKeyAuth": []}],
}


# DJANGO ALIVE - https://github.com/lincolnloop/django-alive/

ALIVE_CHECKS = {"django_alive.checks.check_database": {}, "django_alive.checks.check_cache": {}}


# Anonymizer service settings

EVOTE_SHUFFLE_MULTIPLIER = 3
EVOTE_PAGE_SIZE = 100
EVOTE_MAX_PAGE_SIZE = 10000
