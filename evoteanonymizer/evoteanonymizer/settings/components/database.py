# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from decouple import Choices, config
from get_docker_secret import get_docker_secret

# Database
DB_ENGINE = config(
    "DB_ENGINE",
    cast=Choices([
        "django.db.backends.postgresql",
        "django.db.backends.mysql",
        "django.db.backends.sqlite3",
        "django.db.backends.oracle",
    ]),
    default="django.db.backends.postgresql",
)
DATABASES_OPTIONS = {}

if DB_ENGINE == "django.db.backends.postgresql":
    DATABASES_OPTIONS = {
        "sslmode": config(
            "DB_SSLMODE",
            default="require",
            cast=Choices(["disable", "allow", "prefer", "require", "verify-ca", "verify-full"]),
        )
    }

DATABASES = {
    "default": {
        "ENGINE": DB_ENGINE,
        "NAME": config("DB_NAME"),
        "USER": config("DB_USER"),
        "PASSWORD": get_docker_secret("ANONYMIZER_DB_PASSWORD") or get_docker_secret("DB_PASSWORD", safe=False),
        "HOST": config("DB_HOST", default="localhost"),
        "PORT": config("DB_PORT", default=5432, cast=int),
        "CONN_MAX_AGE": config("DB_CONN_MAX_AGE", default=30, cast=int),
        "OPTIONS": DATABASES_OPTIONS,
    }
}
