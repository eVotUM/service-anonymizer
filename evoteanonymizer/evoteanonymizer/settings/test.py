# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from split_settings.tools import include

include("components/common.py", "components/i18n.py")


# Force language code to avoid problems

LANGUAGES = (("en", "English"),)
LANGUAGE_CODE = "en"


# Use memory cache to run tests faster

CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}

# Database

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}}

# Use simpler hashes to run tests faster

PASSWORD_HASHERS = ("django.contrib.auth.hashers.MD5PasswordHasher",)


# ignore emails

EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"


# LOGGING

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "handlers": {"null": {"class": "logging.NullHandler"}},
    "loggers": {"": {"level": "NOTSET", "handlers": ["null"]}},
}
