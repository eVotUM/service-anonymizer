# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase


class SettingsTest(SimpleTestCase):
    """ """

    def test_base_settings(self):
        """ """
        from evoteanonymizer.settings import test

        self.assertTrue(hasattr(test, "SECRET_KEY"))


class WsgiTest(SimpleTestCase):
    def test_load_wsgi(self):
        from evoteanonymizer.wsgi import application

        self.assertIsNotNone(application._view_middleware)


class UrlsTest(SimpleTestCase):
    def test_load_urls(self):
        from evoteanonymizer import urls

        self.assertIsNotNone(urls)
