# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.db import IntegrityError, models, transaction

from rest_framework import serializers
from rest_framework.settings import api_settings
from rest_framework.utils import model_meta

from .models import Election, ElectionVote, VoteReference


class ElectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Election
        fields = ("id", "election_id", "expected_votes_count")
        read_only_fields = ["id"]


class ElectionVoteSerializer(serializers.ModelSerializer):
    reference = serializers.CharField(help_text="Unique bulletin number", write_only=True)

    class Meta:
        model = ElectionVote
        fields = (
            "id",
            "election",
            "ciphered_vote",
            "blind_components",
            "pr_components",
            "signed_vote_hash",
            "reference",
        )
        read_only_fields = ["id", "election"]

    @transaction.atomic
    def save(self, **kwargs):
        election_id = self.context.get("view").kwargs.get("election_id")
        # Acquire lock based on election to insure that ElectionVote and VoteReference are updated sequentially
        # and avoid race conditions when checking registered votes versus expected votes.
        election = Election.objects.select_for_update().get(election_id=election_id)
        if election.registered_votes_count == election.expected_votes_count:
            raise serializers.ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: ["Impossible to register vote, max expected votings reached."]
            })

        instance = super().save(election=election, **kwargs)
        election.registered_votes_count = models.F("registered_votes_count") + 1
        election.save()

        return instance

    def create(self, validated_data):
        """
        This method is called by save, which is inside a transaction atomic, insuring that there are no
        race conditions to update the election vote and vote reference objects.
        """
        election: Election = validated_data["election"]

        election_vote = election.votes.get_random_empty_entry()
        election_vote.ciphered_vote = validated_data["ciphered_vote"]
        election_vote.signed_vote_hash = validated_data["signed_vote_hash"]
        election_vote.blind_components = validated_data["blind_components"]
        election_vote.pr_components = validated_data["pr_components"]
        election_vote.is_empty = False
        election_vote.save()

        vote_reference = election.vote_references.get_random_empty_entry()
        vote_reference.reference = validated_data["reference"]
        vote_reference.is_empty = False
        vote_reference.save()

        return election_vote


class BulkUpdateListSerializer(serializers.ListSerializer):
    def update(self, instances, validated_data):
        updated_instances = [
            self._update_instance(instance, validated_data[index]) for index, instance in enumerate(instances)
        ]

        try:
            self.child.Meta.model.objects.bulk_update(
                updated_instances, [field.field_name for field in self.child._writable_fields]
            )
        except IntegrityError as e:
            raise serializers.ValidationError(e) from e

        return updated_instances

    def _update_instance(self, instance, validated_data):
        info = model_meta.get_field_info(instance)

        # Simply set each attribute on the instance.
        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                # ignore to many relations
                continue
            else:
                setattr(instance, attr, value)

        return instance


class ElectionVoteUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElectionVote
        fields = ("id", "deciphered_vote", "decipher_timestamp")
        list_serializer_class = BulkUpdateListSerializer


class VoteReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = VoteReference
        fields = ("reference",)
