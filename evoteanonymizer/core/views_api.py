# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import logging

from django.http import Http404
from django.shortcuts import get_object_or_404

from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema, inline_serializer
from rest_framework import status
from rest_framework.generics import CreateAPIView, DestroyAPIView, ListAPIView, ListCreateAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.serializers import IntegerField, Serializer, ValidationError
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from .filters import VoteReferenceFilter
from .models import Election, VoteReference
from .serializers import (
    ElectionSerializer,
    ElectionVoteSerializer,
    ElectionVoteUpdateSerializer,
    VoteReferenceSerializer,
)
from .utils import StandardPagination

logger = logging.getLogger(__name__)


class ElectionCreateView(CreateAPIView):
    """
    Create election and bulk create empty spaces for election votes and vote references.
    """

    serializer_class = ElectionSerializer


class ElectionDestroyView(DestroyAPIView):
    """
    Delete election and remove all related election votes and vote references.
    """

    lookup_field = "election_id"
    lookup_url_kwarg = "election_id"
    queryset = Election.objects.all()
    serializer_class = Serializer  # not needed in normal conditions, this is to avoid errors on drf-spectacular.


class ElectionVoteListCreateView(ListCreateAPIView):
    """
    List or create election votes atomically.
    """

    pagination_class = StandardPagination
    serializer_class = ElectionVoteSerializer

    def post(self, request, *args, **kwargs):
        # Rewrite the method to check if election with provided id exists
        if not Election.objects.filter(election_id=self.kwargs["election_id"]).exists():
            raise Http404("Election not initialized yet.")
        return super().post(request, *args, **kwargs)

    def get_queryset(self):
        election = get_object_or_404(Election, election_id=self.kwargs["election_id"])
        return election.votes.filter(is_empty=False).order_by("pk")


@extend_schema(responses={status.HTTP_200_OK: inline_serializer("ElectionVoteCount", {"count": IntegerField()})})
class ElectionVoteCountView(APIView):
    """
    Partial count of submitted election votes.
    """

    def get(self, *args, **kwargs):
        election = get_object_or_404(Election, election_id=self.kwargs["election_id"])
        return Response({"count": election.votes.filter(is_empty=False).count()})


@extend_schema(request=ElectionVoteUpdateSerializer(many=True), responses={status.HTTP_204_NO_CONTENT: None})
class ElectionVoteBulkUpdateView(UpdateAPIView):
    """
    Bulk update vote references.
    """

    MIN_BULK_UPDATE_INSTANCE = 1
    MAX_BULK_UPDATE_INSTANCE = 3000

    serializer_class = ElectionVoteUpdateSerializer

    def update(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(
            queryset,
            data=request.data,
            many=True,
            min_length=self.MIN_BULK_UPDATE_INSTANCE,
            max_length=self.MAX_BULK_UPDATE_INSTANCE,
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        election = get_object_or_404(Election, election_id=self.kwargs["election_id"])

        if not isinstance(self.request.data, list):
            raise ValidationError({api_settings.NON_FIELD_ERRORS_KEY: ["Expected list of objects to update."]})

        return election.votes.filter(pk__in=[data["id"] for data in self.request.data], is_empty=False)


class VoteReferenceListView(ListAPIView):
    """
    List all available vote references allowing filtering entries.
    """

    filter_backends = (DjangoFilterBackend,)
    pagination_class = StandardPagination
    serializer_class = VoteReferenceSerializer
    filterset_class = VoteReferenceFilter

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return VoteReference.objects.none()

        election = get_object_or_404(Election, election_id=self.kwargs["election_id"])
        return election.vote_references.filter(is_empty=False).order_by("pk")
