# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase

from core.models import Election, ElectionVote, VoteReference


class ElectionTest(SimpleTestCase):
    """ """

    def test_str(self):
        instance = Election(election_id=1)
        self.assertEqual(str(instance), f"<Election id={instance.election_id}>")


class ElectionVoteTest(SimpleTestCase):
    """ """

    def test_str(self):
        instance = ElectionVote(signed_vote_hash="dummy")
        self.assertEqual(str(instance), instance.signed_vote_hash)


class UniqueVoteTest(SimpleTestCase):
    """ """

    def test_str(self):
        instance = VoteReference(reference="dummy")
        self.assertEqual(str(instance), instance.reference)
