# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.utils import timezone
from django.utils.crypto import get_random_string


class CoreTestUtils:
    def create_vote_obj(self):
        return {
            "ciphered_vote": get_random_string(25),
            "signed_vote_hash": get_random_string(128),
            "blind_components": get_random_string(32),
            "pr_components": get_random_string(32),
            "reference": get_random_string(64),
        }

    def update_vote_obj(self, pk):
        return {"id": pk, "deciphered_vote": get_random_string(25), "decipher_timestamp": timezone.now().isoformat()}
