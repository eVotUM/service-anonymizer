# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import override_settings
from django.urls import reverse_lazy

from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from core.models import Election, ElectionVote, VoteReference

from .utils import CoreTestUtils


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class ElectionCreateViewTest(APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.url = reverse_lazy("api:election_create")
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_create(self):
        response = self.client.post(self.url, data={"election_id": 1, "expected_votes_count": 5}, headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ElectionVote.objects.count(), 15)
        self.assertEqual(VoteReference.objects.count(), 15)

    def test_create_not_allowed_duplicated_election_id(self):
        self.client.post(self.url, data={"election_id": 1, "expected_votes_count": 1}, headers=self.headers)
        response = self.client.post(self.url, data={"election_id": 1, "expected_votes_count": 1}, headers=self.headers)
        self.assertEqual(response.status_code, 400)
        self.assertIn("election_id", response.data)
        self.assertEqual(response.data["election_id"][0], "Election with this election id already exists.")

    def test_create_without_api_key(self):
        response = self.client.post(self.url, data={"election_id": 1, "expected_votes_count": 5})
        self.assertEqual(response.status_code, 403)


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class ElectionDestroyViewTest(APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.election = Election.objects.create(election_id=1, expected_votes_count=1)
        self.url = reverse_lazy("api:election_destroy", kwargs={"election_id": self.election.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_destroy(self):
        response = self.client.delete(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 204)

    def test_destroy_not_existent_election_id(self):
        response = self.client.delete(
            reverse_lazy("api:election_destroy", kwargs={"election_id": 10}), headers=self.headers
        )
        self.assertEqual(response.status_code, 404)

    def test_destroy_without_api_key(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, 403)


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class ElectionVoteListCreateViewTest(CoreTestUtils, APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.election = Election.objects.create(election_id=1, expected_votes_count=1)
        self.url = reverse_lazy("api:election_vote_list_create", kwargs={"election_id": self.election.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_list_not_existent_election(self):
        response = self.client.get(
            reverse_lazy("api:election_vote_list_create", kwargs={"election_id": 2}), headers=self.headers
        )
        self.assertEqual(response.status_code, 404)

    def test_list_all_empty_entries(self):
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 0)

    def test_list(self):
        self.election.votes.update(is_empty=False)
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 3)

    def test_list_without_api_key(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_create_not_existent_election(self):
        response = self.client.post(
            reverse_lazy("api:election_vote_list_create", kwargs={"election_id": 2}),
            data=self.create_vote_obj(),
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 404)

    def test_create(self):
        response = self.client.post(self.url, data=self.create_vote_obj(), headers=self.headers)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.election.votes.filter(is_empty=False).count(), 1)
        self.assertEqual(self.election.vote_references.filter(is_empty=False).count(), 1)

    def test_create_max_entries_reached(self):
        response = self.client.post(self.url, data=self.create_vote_obj(), headers=self.headers)
        self.assertEqual(response.status_code, 201)
        response = self.client.post(self.url, data=self.create_vote_obj(), headers=self.headers)
        self.assertEqual(response.status_code, 400)
        self.assertIn("non_field_errors", response.data)
        self.assertEqual(
            response.data["non_field_errors"][0], "Impossible to register vote, max expected votings reached."
        )

    def test_create_without_api_key(self):
        response = self.client.post(self.url, data=self.create_vote_obj())
        self.assertEqual(response.status_code, 403)


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class ElectionVoteCountViewTest(APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.election = Election.objects.create(election_id=1, expected_votes_count=2)
        self.url = reverse_lazy("api:election_vote_count", kwargs={"election_id": self.election.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_get_partial_count_not_existent_election(self):
        response = self.client.get(
            reverse_lazy("api:election_vote_count", kwargs={"election_id": 2}), headers=self.headers
        )
        self.assertEqual(response.status_code, 404)

    def test_get_partial_count_no_votes(self):
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertIn("count", response.data)
        self.assertEqual(response.data["count"], 0)

    def test_get_partial_count_registered_votes(self):
        self.election.votes.update(is_empty=False)
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertIn("count", response.data)
        self.assertEqual(response.data["count"], 6)

    def test_get_partial_count_without_api_key(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class ElectionVoteBulkUpdateViewTest(CoreTestUtils, APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.election = Election.objects.create(election_id=1, expected_votes_count=2)
        self.url = reverse_lazy("api:election_vote_bulk_update", kwargs={"election_id": self.election.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_update_not_existent_election(self):
        response = self.client.put(
            reverse_lazy("api:election_vote_bulk_update", kwargs={"election_id": 2}), {}, headers=self.headers
        )
        self.assertEqual(response.status_code, 404)

    def test_update_with_wrong_type_data_sent(self):
        response = self.client.put(self.url, {"wrong": "datatype"}, headers=self.headers)
        self.assertIn("non_field_errors", response.data)
        self.assertEqual(response.data["non_field_errors"][0], "Expected list of objects to update.")

    def test_update_empty_election_votes(self):
        election_vote = self.election.votes.filter(is_empty=True).first()
        response = self.client.put(self.url, [self.update_vote_obj(election_vote.pk)], headers=self.headers)
        self.assertEqual(response.status_code, 204)

    def test_update_election_votes(self):
        self.election.votes.filter(is_empty=True).update(is_empty=False)
        queryset = self.election.votes.filter(is_empty=False)
        response = self.client.put(
            self.url,
            [self.update_vote_obj(queryset.first().pk), self.update_vote_obj(queryset.last().pk)],
            headers=self.headers,
        )
        self.assertEqual(response.status_code, 204)

    def test_update_election_without_api_key(self):
        self.election.votes.filter(is_empty=True).update(is_empty=False)
        queryset = self.election.votes.filter(is_empty=False)
        response = self.client.put(
            self.url, [self.update_vote_obj(queryset.first().pk), self.update_vote_obj(queryset.last().pk)]
        )
        self.assertEqual(response.status_code, 403)


@override_settings(EVOTE_SHUFFLE_MULTIPLIER=3)
class VoteReferenceListViewTest(APITestCase):
    def setUp(self):
        _, key = APIKey.objects.create_key(name="test_key")
        self.election = Election.objects.create(election_id=1, expected_votes_count=1)
        self.url = reverse_lazy("api:vote_reference_list", kwargs={"election_id": self.election.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_list_not_existent_election(self):
        response = self.client.get(
            reverse_lazy("api:vote_reference_list", kwargs={"election_id": 2}), headers=self.headers
        )
        self.assertEqual(response.status_code, 404)

    def test_list_all_empty_entries(self):
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 0)

    def test_list(self):
        self.election.vote_references.update(is_empty=False)
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 3)

    def test_list_filter_by_reference(self):
        self.election.vote_references.update(is_empty=False)
        vote_reference = self.election.vote_references.first()
        vote_reference.reference = "ref to test"
        vote_reference.save()
        response = self.client.get(self.url, {"reference": "ref"}, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)

    def test_list_without_api_key(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
