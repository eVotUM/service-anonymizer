# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.db import models, transaction

from .managers import CommonManager


class Election(models.Model):
    election_id = models.IntegerField("election id", db_index=True, unique=True)
    expected_votes_count = models.PositiveIntegerField("how many votes are expected?")
    registered_votes_count = models.PositiveIntegerField(
        "how many votes have been registered?", default=0, editable=False
    )

    class Meta:
        verbose_name = "Election"
        verbose_name_plural = "Elections"

    def __str__(self):
        return f"<Election id={self.election_id}>"

    @transaction.atomic
    def save(self, *args, **kwargs):
        adding = self.pk is None
        super().save(*args, **kwargs)
        if adding is True:
            ElectionVote.objects.initialize(self.expected_votes_count, election=self)
            VoteReference.objects.initialize(self.expected_votes_count, election=self)


class ElectionVote(models.Model):
    election = models.ForeignKey(Election, related_name="votes", on_delete=models.CASCADE)
    is_empty = models.BooleanField("empty entry", default=True)
    ciphered_vote = models.TextField("ciphered vote", default="")
    blind_components = models.TextField("blind components", default="")
    pr_components = models.TextField("pRcomponents", default="")
    signed_vote_hash = models.TextField("signed vote hash", default="")
    deciphered_vote = models.TextField("deciphered vote", default="")
    decipher_timestamp = models.DateTimeField("deciphered vote date", null=True, blank=True)

    objects: CommonManager = CommonManager()

    class Meta:
        verbose_name = "Election vote"
        verbose_name_plural = "Election votes"

    def __str__(self):
        return self.signed_vote_hash


class VoteReference(models.Model):
    election = models.ForeignKey(Election, related_name="vote_references", on_delete=models.CASCADE)
    is_empty = models.BooleanField("empty entry", default=True)
    reference = models.CharField("reference", max_length=512, db_index=True, default="", editable=False)

    objects: CommonManager = CommonManager()

    class Meta:
        verbose_name = "Unique election reference"
        verbose_name_plural = "Unique election references"

    def __str__(self):
        return self.reference
