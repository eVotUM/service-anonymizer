# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.conf import settings
from django.db import models
from django.db.models.functions import Floor, Random


class BaseQuerySet(models.QuerySet):
    def get_random_empty_entry(self):
        """
        Return a random empty entry.

        This is not concurrency safe by default. To avoid race conditions, please use this method in conjunction
        with select_for_update or make sure to use it with locks.
        """
        qs = self.filter(is_empty=True)
        return qs.get(pk=qs._get_random_id())

    def _get_random_id(self):
        """
        Return a random id in most efficient way possible.
        https://stackoverflow.com/a/5297523
        """
        return (
            self.annotate(random_offset=Floor(Random() * self.count()))
            .order_by("random_offset")
            .values_list("pk", flat=True)[:1][0]
        )


class BaseManager(models.Manager):
    def initialize(self, total_entries, **kwargs):
        return self.bulk_create(
            [self.model(**kwargs) for _i in range(total_entries * settings.EVOTE_SHUFFLE_MULTIPLIER)], batch_size=3000
        )


CommonManager = BaseManager.from_queryset(BaseQuerySet)
