%{!?enviroment:%define enviroment production}
%global _python_bytecompile_errors_terminate_build 0
%define  debug_package %{nil}

Name:           evote-anonymizer
Version:        0.6.2
Release:        1%{?dist}
Summary:        anonymizer eVote service

Group:          Applications/Internet
License:        GPLv2+
URL:            http://eurotux.com
Source0:        sources-anonymizer.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python
BuildRequires: python-pip
BuildRequires: mariadb-devel
BuildRequires: openssl-devel
BuildRequires: libffi-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: freetype-devel
BuildRequires: python-virtualenv
BuildRequires: gcc
BuildRequires: gcc-c++
Requires: mariadb
Requires: python-virtualenv
Requires: python-pip
Requires: supervisor
Requires: nginx

AutoReqProv:	no

%description
anonymizer eVote service


%prep
%setup -q -n sources-anonymizer


%build


%install
rm -rf $RPM_BUILD_ROOT
# Create all directories
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-anonymizer/docs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-anonymizer/data
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-anonymizer/logs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-anonymizer/run
# Copy code to final path
cp -r evoteanonymizer $RPM_BUILD_ROOT/srv/et/evote-anonymizer/
# Copy sripts to final path
cp -r builds/scripts $RPM_BUILD_ROOT/srv/et/evote-anonymizer/
# Copy config to final path
cp -r builds/config.env $RPM_BUILD_ROOT/srv/et/evote-anonymizer/
# Install all python dependencies with pip
cd $RPM_BUILD_ROOT/srv/et/evote-anonymizer/
virtualenv env
env/bin/pip install pip --upgrade
env/bin/pip install setuptools --upgrade
env/bin/pip install -r evoteanonymizer/evoteanonymizer/requirements/%{enviroment}.txt;
echo "source /srv/et/evote-anonymizer/config.env" >> env/bin/activate
# Remove RPM_BUILD_ROOT string from files
sed -i "s|${RPM_BUILD_ROOT}||g" env/bin/*
# Add script service to nginx (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/nginx/conf.d
ln -s /srv/et/evote-anonymizer/scripts/evote.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/evote.conf
# Add script service to supervisor (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/supervisord.d
ln -s /srv/et/evote-anonymizer/scripts/supervisor.ini $RPM_BUILD_ROOT/etc/supervisord.d/evote.ini


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/srv/et/evote-anonymizer/docs
/srv/et/evote-anonymizer/data
/srv/et/evote-anonymizer/logs
/srv/et/evote-anonymizer/run
/srv/et/evote-anonymizer/evoteanonymizer
/srv/et/evote-anonymizer/env
/srv/et/evote-anonymizer/scripts/install.sh
/srv/et/evote-anonymizer/scripts/*_start
%config(noreplace) /srv/et/evote-anonymizer/config.env
%config(noreplace) /srv/et/evote-anonymizer/scripts/evote.conf
%config(noreplace) /srv/et/evote-anonymizer/scripts/supervisor.ini
%config(noreplace) /etc/nginx/conf.d/evote.conf
%config(noreplace) /etc/supervisord.d/evote.ini


%post
# Code for install rpm
if [ "$1" == "1" ]; then
    useradd evote
    chown -R evote:evote /srv/et/evote-anonymizer

    systemctl enable nginx.service
    systemctl start nginx.service

    systemctl enable supervisord.service
    systemctl start supervisord.service
    supervisorctl update
    supervisorctl start evote:

    echo "To finish the instalation use the command '/srv/et/evote-anonymizer/scripts/install.sh'"

# Code for update rpm
else
    chown -R evote:evote /srv/et/evote-anonymizer
    systemctl reload nginx.service
    supervisorctl restart evote:
fi


%changelog
* Fri Mar 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.6.2-1
- Removed localpkgs dependency;
- Removed directories cache and backup;

* Wed Jan 25 2017 André Rocha <asr@eurotux.com> 0.6.1-1
- Changed closeelection API endpoint to clear all existing election entries
- Replaced gracefull reload of gunicorn to restart
- chown in rpm update

* Mon Jan 9 2017 André Rocha <asr@eurotux.com> 0.6-2
- Changed initialize_election error to boolean response

* Sun Jan 8 2017 André Rocha <asr@eurotux.com> 0.6-1
- Finished unit test coverage
- Added Ping API endpoint

* Fri Jan 6 2017 Luis Silva <lms@eurotux.com> 0.5.1-4
- Bug fixed in install.sh

* Thu Jan 5 2017 Luis Silva <lms@eurotux.com> 0.5.1-3
- Bug fixed in install.sh

* Wed Jan 4 2017 Luis Silva <lms@eurotux.com> 0.5.1-2
- Created new release

* Tue Jan 3 2017 Luis Silva <lms@eurotux.com> 0.5.1-1
- Updated source code
- Removed redis from this package

* Tue Dec 27 2016 André Rocha <asr@eurotux.com> 0.5-3
- Refactored anonymizevote endpoint to be election_id dependent

* Mon Dec 19 2016 André Rocha <asr@eurotux.com> 0.5-2
- Post-install fixes

* Thu Dec 15 2016 André Rocha <asr@eurotux.com> 0.5-1
- First installable version

* Wed Dec 14 2016 André Rocha <asr@eurotux.com> 0.4-2
- Anonymizer Service refactoring

* Wed Nov 09 2016 Carlos Rodrigues <cmar@eurotux.com> 0.1-1
- anonymizer eVote service first version
