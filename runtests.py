#!/usr/bin/env python
import os
import sys
from pathlib import Path

import django
from django.conf import settings
from django.test.utils import get_runner

if __name__ == "__main__":
    project_dir = str(Path(__file__).absolute().parent.joinpath("evoteanonymizer"))

    if project_dir not in sys.path:
        sys.path.append(project_dir)

    os.environ["PYTHONWARNINGS"] = "ignore"
    os.environ["DJANGO_SETTINGS_MODULE"] = "evoteanonymizer.settings.test"
    os.environ["DJANGO_SECRET_KEY"] = "-kll-u!+nvn&4&jf1osp79*6-1-ikoz3)0ogq=u++ox%_+lu2w"  # NOQA: S105
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(["evoteanonymizer"])
    sys.exit(bool(failures))
